package com.vw.cn.customDomain;

import java.util.Date;
/**
 * @author karthikeyan_v
 */
public class DealerCityProvince {

	private long id;

	private String  city_code;

	private String  city_name;

	private String province_code;

	private String province_name;

	private String name;

	private String full_name;

	private String address;

	private String dealer_type_code;

	private Date created_date;

	private Date update_date;

	private float lattitude;

	private float longitude;

	private String website_url;

	private String promotion_image_url;

	private String promotion_page_url;

	private String sales_hotline;

	private String customer_hotline;

	private String after_sales;

	private String postal_code;

	private String lang_code;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCity_code() {
		return city_code;
	}

	public void setCity_code(String city_code) {
		this.city_code = city_code;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public String getProvince_code() {
		return province_code;
	}

	public void setProvince_code(String province_code) {
		this.province_code = province_code;
	}

	public String getProvince_name() {
		return province_name;
	}

	public void setProvince_name(String province_name) {
		this.province_name = province_name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDealer_type_code() {
		return dealer_type_code;
	}

	public void setDealer_type_code(String dealer_type_code) {
		this.dealer_type_code = dealer_type_code;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public float getLattitude() {
		return lattitude;
	}

	public void setLattitude(float lattitude) {
		this.lattitude = lattitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public String getWebsite_url() {
		return website_url;
	}

	public void setWebsite_url(String website_url) {
		this.website_url = website_url;
	}

	public String getPromotion_image_url() {
		return promotion_image_url;
	}

	public void setPromotion_image_url(String promotion_image_url) {
		this.promotion_image_url = promotion_image_url;
	}

	public String getPromotion_page_url() {
		return promotion_page_url;
	}

	public void setPromotion_page_url(String promotion_page_url) {
		this.promotion_page_url = promotion_page_url;
	}

	public String getSales_hotline() {
		return sales_hotline;
	}

	public void setSales_hotline(String sales_hotline) {
		this.sales_hotline = sales_hotline;
	}

	public String getCustomer_hotline() {
		return customer_hotline;
	}

	public void setCustomer_hotline(String customer_hotline) {
		this.customer_hotline = customer_hotline;
	}

	public String getAfter_sales() {
		return after_sales;
	}

	public void setAfter_sales(String after_sales) {
		this.after_sales = after_sales;
	}

	public String getPostal_code() {
		return postal_code;
	}

	public void setPostal_code(String postal_code) {
		this.postal_code = postal_code;
	}

	public String getLang_code() {
		return lang_code;
	}

	public void setLang_code(String lang_code) {
		this.lang_code = lang_code;
	}		
}