package com.vw.cn.customDomain;

import java.util.Date;
/**
 * @author karthikeyan_v
 */
public class DealerRecordList {

	private long id;

	private String province_name;

	private String city_name;

	private String district_name;

	private Date Appln_date;

	private String Appln_status;

	private String  remarks;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProvince_name() {
		return province_name;
	}

	public void setProvince_name(String province_name) {
		this.province_name = province_name;
	}

	public String getCity_name() {
		return city_name;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public String getDistrict_name() {
		return district_name;
	}

	public void setDistrict_name(String district_name) {
		this.district_name = district_name;
	}

	public Date getAppln_date() {
		return Appln_date;
	}

	public void setAppln_date(Date appln_date) {
		Appln_date = appln_date;
	}

	public String getAppln_status() {
		return Appln_status;
	}

	public void setAppln_status(String appln_status) {
		Appln_status = appln_status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}	
}