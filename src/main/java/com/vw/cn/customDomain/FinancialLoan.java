package com.vw.cn.customDomain;

import java.util.Date;
/**
 * @author karthikeyan_v
 */
public class FinancialLoan {
	
	private long id;
	private String modal_Code;
	private String financial_product_code;
	private float rr_price;
	private float down_payment_rate;
	private float loan_tenure;
	private float down_payment_amount;
	private float loan_amount;
	private float monthly_installment;
	private float fee_rate;
	private float customer_rate;
	private Date created_date;
	private float interest_amount;
	private String model_name;
	private String series_name;
	private String financial_product_name;
	private String financial_institution_name;
	private String financial_Institution_code;
	private String series_code;
	public String getSeries_code() {
		return series_code;
	}
	public void setSeries_code(String series_code) {
		this.series_code = series_code;
	}
	public String getFinancial_Institution_code() {
		return financial_Institution_code;
	}
	public void setFinancial_Institution_code(String financial_Institution_code) {
		this.financial_Institution_code = financial_Institution_code;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getModal_Code() {
		return modal_Code;
	}
	public void setModal_Code(String modal_Code) {
		this.modal_Code = modal_Code;
	}
	public String getFinancial_product_code() {
		return financial_product_code;
	}
	public void setFinancial_product_code(String financial_product_code) {
		this.financial_product_code = financial_product_code;
	}
	public float getRr_price() {
		return rr_price;
	}
	public void setRr_price(float rr_price) {
		this.rr_price = rr_price;
	}
	public float getDown_payment_rate() {
		return down_payment_rate;
	}
	public void setDown_payment_rate(float down_payment_rate) {
		this.down_payment_rate = down_payment_rate;
	}
	public float getLoan_tenure() {
		return loan_tenure;
	}
	public void setLoan_tenure(float loan_tenure) {
		this.loan_tenure = loan_tenure;
	}
	public float getDown_payment_amount() {
		return down_payment_amount;
	}
	public void setDown_payment_amount(float down_payment_amount) {
		this.down_payment_amount = down_payment_amount;
	}
	public float getLoan_amount() {
		return loan_amount;
	}
	public void setLoan_amount(float loan_amount) {
		this.loan_amount = loan_amount;
	}
	public float getMonthly_installment() {
		return monthly_installment;
	}
	public void setMonthly_installment(float monthly_installment) {
		this.monthly_installment = monthly_installment;
	}
	public float getFee_rate() {
		return fee_rate;
	}
	public void setFee_rate(float fee_rate) {
		this.fee_rate = fee_rate;
	}
	public float getCustomer_rate() {
		return customer_rate;
	}
	public void setCustomer_rate(float customer_rate) {
		this.customer_rate = customer_rate;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public float getInterest_amount() {
		return interest_amount;
	}
	public void setInterest_amount(float interest_amount) {
		this.interest_amount = interest_amount;
	}
	public String getModel_name() {
		return model_name;
	}
	public void setModel_name(String model_name) {
		this.model_name = model_name;
	}
	public String getSeries_name() {
		return series_name;
	}
	public void setSeries_name(String series_name) {
		this.series_name = series_name;
	}
	public String getFinancial_product_name() {
		return financial_product_name;
	}
	public void setFinancial_product_name(String financial_product_name) {
		this.financial_product_name = financial_product_name;
	}
	public String getFinancial_institution_name() {
		return financial_institution_name;
	}
	public void setFinancial_institution_name(String financial_institution_name) {
		this.financial_institution_name = financial_institution_name;
	}	
}