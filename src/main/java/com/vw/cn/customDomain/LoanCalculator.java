package com.vw.cn.customDomain;

public class LoanCalculator {

	private float total_amount;

	private float down_payment;

	private float emi;

	private float interest;

	public float getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(float total_amount) {
		this.total_amount = total_amount;
	}
	public float getDown_payment() {
		return down_payment;
	}
	public void setDown_payment(float down_payment) {
		this.down_payment = down_payment;
	}
	public float getEmi() {
		return emi;
	}
	public void setEmi(float emi) {
		this.emi = emi;
	}
	public float getInterest() {
		return interest;
	}
	public void setInterest(float interest) {
		this.interest = interest;
	}
}