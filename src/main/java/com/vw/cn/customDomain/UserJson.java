package com.vw.cn.customDomain;

public class UserJson {

	private String account;

	private String password;

	private String confirm_password;

	private boolean  agreed_tc;

	private String verification_code;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirm_password() {
		return confirm_password;
	}

	public void setConfirm_password(String confirm_password) {
		this.confirm_password = confirm_password;
	}

	public boolean isAgreed_tc() {
		return agreed_tc;
	}

	public void setAgreed_tc(boolean agreed_tc) {
		this.agreed_tc = agreed_tc;
	}

	public String getVerification_code() {
		return verification_code;
	}

	public void setVerification_code(String verification_code) {
		this.verification_code = verification_code;
	}


}
