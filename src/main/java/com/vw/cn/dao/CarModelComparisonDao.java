package com.vw.cn.dao;

import com.vw.cn.domain.CarModel;
import com.vw.cn.domain.CarModelParameter;
import com.vw.cn.domain.CarParameter;
import com.vw.cn.domain.CarParameterType;
import com.vw.cn.domain.CarSeries;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.One;

@Mapper
public abstract interface CarModelComparisonDao
{
	public static final String GET_CAR_MODEL_BY_CODE_AND_LANG = "select * from VW_MODEL cm where cm.CODE=#{model_code} and cm.LANG=#{lang_code} and cm.DELETE_FLAG=0";
	public static final String GET_CAR_PARAMETER_BY_CODE_AND_TYPE_AND_LANG = "select * from VW_CAR_PARAMETER cp where cp.CODE =#{param_code} "
			+ "and cp.TYPE=#{param_type} and LANG_CODE=#{lang_code}";
	
	public static final String GET_CAR_MODEL_PARAMETERS_BY_MODEL_AND_TYPE = "select cmp.ID as id, cmp.MODEL_CODE as model_code, cmp.PARAM_VALUE as value, "
			+ " cmp.PARAM_CODE as param_code, cmp.PARAM_CATEGORY as category, cmp.IMAGE_URL as imageUrl , "
			+ " cmp.IS_COMPARABLE as is_comparable, cmp.DISPLAY_IMAGE as display_image, cp.TYPE as param_type, cp.SHOW_CIRCLE as show_circle"
			+ " from VW_CAR_MODEL_PARAMETER cmp "
			+ " inner join VW_CAR_PARAMETER cp on cp.CODE=cmp.PARAM_CODE "
			+ " where cmp.MODEL_CODE=#{model_code} and cp.LANG_CODE=#{lang_code} and cp.TYPE=#{param_type}";

	@Select({GET_CAR_MODEL_BY_CODE_AND_LANG})
	@Results({
		@Result(property="id", column="ID"), 
		@Result(property="code", column="CODE"), 
		@Result(property="name", column="NAME"),
		@Result(property="series_code", column="SERIES_CODE"), 
		@Result(property="lang", column="LANG"), 
		@Result(property="currency_code", column="CURRENCY_CODE"), 
		@Result(property="recommended_retail_price", column="RECOMMENDED_RETAIL_PRICE"), 
		@Result(property="image_url", column="IMAGE_URL"), 
		@Result(property="delete_flag", column="DELETE_FLAG"), 
		@Result(property="update_time", column="UPDATE_TIME")})
	public abstract CarModel getCarModelByCodeAndLanguage(@Param("model_code") String model_code, @Param("lang_code") String lang_code);

	@Select({GET_CAR_PARAMETER_BY_CODE_AND_TYPE_AND_LANG})
	@Results({
		@Result(property="id", column="ID"), 
		@Result(property="code", column="CODE"), 
		@Result(property="type", column="TYPE"), 
		@Result(property="name", column="NAME"), 
		@Result(property="lang", column="LANG_CODE"), 
		@Result(property="displayCircle", column="SHOW_CIRCLE"),
		@Result(property="description", column="DESCRIPTION")})
	public abstract CarParameter getCarParameterByCodeLangAndType(@Param("param_type") String param_type, 
			@Param("lang_code") String lang_code, @Param("type") String type);

/*	@Select({GET_CAR_MODEL_PARAMETERS_BY_MODEL_AND_TYPE})
	@Results({
		@Result(property="id", column="ID"), 
		@Result(property="model_code", column="MODEL_CODE"), 
		@Result(property="param_code", column="PARAM_CODE"), 
		@Result(property="lang", column="LANG_CODE"), 
		@Result(property="value", column="PARAM_VALUE"), 
		@Result(property="category", column="PARAM_CATEGORY"), 
		@Result(property="imageUrl", column="IMAGE_URL"), 
		@Result(property="comparable", column="IS_COMPARABLE"), 
		@Result(property="displayImage", column="DISPLAY_IMAGE"), 
		@Result(property="model", column="{CODE=model_code,LANG=lang}", javaType=CarModel.class, one=@One(select="com.vw.cn.dao.CarModelComparisonDao.getCarModelByCodeAndLanguage")), 
		@Result(property="carParameter", column="{CODE=param_code,LANG_CODE=lang }", javaType=CarParameter.class, one=@One(select="com.vw.cn.dao.CarModelComparisonDao.getCardParameterByCodeAndLanguage"))})
	public abstract List<CarModelParameter> getCarParametersByModelAndType(@Param("model_code") String model_code, 
			@Param("param_type") String param_type, @Param("lang_code") String lang_code);*/
	
	@Select({GET_CAR_MODEL_PARAMETERS_BY_MODEL_AND_TYPE})
	@Results({
		@Result(property="id", column="id"), 
		@Result(property="model_code", column="model_code"), 
		@Result(property="param_code", column="param_code"), 		
		@Result(property="value", column="value"), 
		@Result(property="category", column="category"), 
		@Result(property="imageUrl", column="imageUrl"), 
		@Result(property="comparable", column="is_comparable"), 
		@Result(property="displayImage", column="display_image"),
		@Result(property="param_type", column="param_type"),
		@Result(property="show_circle", column="show_circle")})
	public abstract List<CarModelParameter> getCarParametersByModelAndType(@Param("model_code") String model_code, 
			@Param("param_type") String param_type, @Param("lang_code") String lang_code);
	
	
	@Select(value = {"select * from VW_SERIES where CODE =#{code} and LANG=#{lang_code}"})
	CarSeries findCarSeriesByCodeAndLang(@Param("code") String code, @Param("lang_code") String lang_code);
	
	/**
	 * To retrieve the parameter type by language
	 * @param lang_code
	 * @return the CarParameterType list
	 */
	@Select(value = {"select * from VW_PARAMETER_TYPE where LANG=#{lang_code}"})		
	List<CarParameterType> findCarParameterTypesByLang(String lang_code);

	/**
	 * To retrieve the parameter by type and language
	 * @param type
	 * @param lang_code
	 * @return the CarParameter list
	 */
	@Select(value = {"select * from VW_CAR_PARAMETER where TYPE=#{param_type} AND LANG_CODE=#{lang_code}"})
	List<CarParameter> findCarParametersByTypeAndLang(@Param("param_type") String type, @Param("lang_code") String lang_code);	
}