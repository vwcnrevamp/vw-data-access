package com.vw.cn.dao;
/**
 * @author karthikeyan_v
 */
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.customDomain.DealerCityProvince;
import com.vw.cn.customDomain.DealerRecordList;
import com.vw.cn.domain.Dealer;
import com.vw.cn.domain.DealerContact;
import com.vw.cn.domain.DealerRecruitingCity;
import com.vw.cn.domain.DealerRecruitment;
import com.vw.cn.domain.Promotion;
import com.vw.cn.domain.SearchDealer;
import com.vw.cn.domain.ServiceRating;

@Mapper
public interface DealerDao
{
	@Insert(value = {"insert into VW_DEALER(CITY,PROVINCE,NAME,FULL_NAME,ADDRESS,DEALER_TYPE_CODE,CREATED_DATE,UPDATE_DATE,LATTITUDE,LONGITUDE,WEBSITE_URL,PROMOTION_IMAGE_URL,PROMOTION_PAGE_URL,SALES_HOTLINE,CUSTOMER_HOTLINE,AFTER_SALES,POSTAL_CODE) values(#{city},#{province},#{name},#{full_name},#{address},#{dealer_type_code},#{created_date},#{update_date},#{lattitude},#{longitude},#{website_url},#{promotion_image_url},#{promotion_page_url},#{sales_hotline},#{customer_hotline},#{after_sales},#{postal_code})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertDealer(Dealer dealerRegistration);

	@Update(value = {"update VW_DEALER set CITY=#{city},PROVINCE=#{province},NAME=#{name},FULL_NAME=#{full_name},ADDRESS=#{address},DEALER_TYPE_CODE=#{dealer_type_code},CREATED_DATE=#{created_date},UPDATE_DATE=#{update_date},LATTITUDE=#{lattitude},LONGITUDE=#{longitude},WEBSITE_URL=#{website_url},PROMOTION_IMAGE_URL=#{promotion_image_url},PROMOTION_PAGE_URL=#{promotion_page_url},SALES_HOTLINE=#{sales_hotline},CUSTOMER_HOTLINE=#{customer_hotline},AFTER_SALES=#{after_sales},POSTAL_CODE=#{postal_code} where ID=#{id}"})
	void updateDealer(Dealer dealerRegistration);

	@Select(value = {"select * from VW_DEALER WHERE ID=#{id}"})
	Dealer findDealerById(long id);

	@Select(value = {"select * from VW_DEALER WHERE PROVINCE=#{prv_code}"})
	List<Dealer> findDealerByCode(String prv_code);

	@Select(value = {"select * from VW_DEALER"})
	List<Dealer> findAllDealers();	

	//@Delete(value = {"delete from VW_DEALER_REGISTRATION where ID=#{id}"})
	@Update(value = {"update VW_DEALER set DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void deleteDealer(Dealer dealer);

	//## Dealer Contact
	@Insert(value = {"insert into VW_DEALER_CONTACT(NAME,PHONE,EMAIL,JOB_TITLE,CITY,LAST_MODIFIED_BY,DEALER_ID,CREATED_DATETIME,LAST_MODIFIED_DATETIME,CONTACT_TYPE) values(#{name},#{phone},#{email},#{job_title},#{city},#{last_modified_by},#{dealer_id},#{created_date},#{last_modified_date},#{contact_type})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertDealerContact(DealerContact dealerRegistrationContact);

	@Update(value = {"update VW_DEALER_CONTACT set NAME=#{name},PHONE=#{phone},EMAIL=#{email},JOB_TITLE=#{job_title},CITY=#{city},LAST_MODIFIED_BY=#{last_modified_by},DEALER_ID=#{dealer_id},CREATED_DATETIME=#{created_date},LAST_MODIFIED_DATETIME=#{last_modified_date},CONTACT_TYPE=#{contact_type} where ID=#{id}"})
	void updateDealerContact(DealerContact dealerRegistrationContact);

	@Select(value = {"select * from VW_DEALER_CONTACT WHERE ID=#{id}"})
	DealerContact findDealerContactById(long id);

	@Select(value = {"select * from VW_DEALER_CONTACT"})
	List<DealerContact> findAllDealerContacts();	

	@Delete(value = {"delete from VW_DEALER_CONTACT where ID=#{id}"})
	void deleteDealerContact(long id);	

	//## Promotion
	@Insert(value = {"insert into VW_DEALER_PROMOTION(DEALER_ID,PROMOTIONAL_BANNER_IMAGE_URL,PROMOTION_PAGE_URL,CREATED_DATE,VALID_FROM,VALID_TILL) values(#{dealer_id},#{promotional_banner_image_url},#{promotion_page_url},#{created_date},#{valid_from},#{valid_till})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertPromotion(Promotion promotion);

	@Update(value = {"update VW_DEALER_PROMOTION set DEALER_ID=#{dealer_id},PROMOTIONAL_BANNER_IMAGE_URL=#{promotional_banner_image_url},PROMOTION_PAGE_URL=#{promotion_page_url},CREATED_DATE=#{created_date},VALID_FROM=#{valid_from},VALID_TILL=#{valid_till} where ID=#{id}"})
	void updatePromotion(Promotion promotion);

	@Select(value = {"select * from VW_DEALER_PROMOTION WHERE ID=#{id}"})
	Promotion findPromotionById(long id);

	@Select(value = {"select * from VW_DEALER_PROMOTION"})
	List<Promotion> findAllPromotions();	

	@Delete(value = {"delete from VW_DEALER_PROMOTION where ID=#{id}"})
	void deletePromotion(long id);		

	//## Service Rating
	@Insert(value = {"insert into VW_DEALER_RATING(USER_ID,DEALER_ID,SERVICE_RATING,RATED_ON,DESCRIPTION) values(#{user_id},#{dealer_id},#{service_rating},#{rated_on},#{description})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertServiceRating(ServiceRating serviceRating);

	@Update(value = {"update VW_DEALER_RATING set USER_ID=#{user_id},DEALER_ID=#{dealer_id},SERVICE_RATING=#{service_rating},RATED_ON=#{rated_on},DESCRIPTION=#{description} where ID=#{id}"})
	void updateServiceRating(ServiceRating serviceRating);

	@Select(value = {"select * from VW_DEALER_RATING WHERE ID=#{id}"})
	ServiceRating findServiceRatingById(long id);

	@Select(value = {"select * from VW_DEALER_RATING"})
	List<ServiceRating> findAllServiceRatings();	

	@Delete(value = {"delete from VW_DEALER_RATING where ID=#{id}"})
	void deleteServiceRating(long id);	
	
	//Search Dealer
	@Select(value = {"select "
			+ "d.ID as id,d.CITY as city_code,d.PROVINCE as province_code,d.LATTITUDE as latitude,d.LONGITUDE as longitude,d.ADDRESS as formatted_address,d.WEBSITE_URL as website_url, d.DEALER_TYPE_CODE as dealer_type,dc.NAME as name, "
			+ "d.PROMOTION_IMAGE_URL as promotional_banner_image_url,d.PROMOTION_PAGE_URL as promotion_page_url, "
			+ "d.SALES_HOTLINE as contact_sales_hotline,d.CUSTOMER_HOTLINE as contact_customer_hotline,d.AFTER_SALES as contact_after_sales "
			+ "c.NAME as city_name, "
			+ "p.NAME as province_name, "
			+ "FROM VW_DEALER d "
			+ "INNER JOIN VW_CITY c on c.CODE=d.DECLARED_CITY "
			+ "INNER JOIN VW_PROVINCE p on p.CODE=d.DECLARED_PROVINCE "		
			+ "where "
			+ "d.DECLARED_CITY=#{city_code} and d.DECLARED_PROVINCE=#{province_code} and d.DEALER_TYPE_CODE=#{dealer_type}"})
	List<SearchDealer> searchDealer(SearchDealer dealer);	

	//## Dealer Recruiting City
	@Insert(value = {"insert into VW_DEALER_RECRUITING_CITY(CITY,PROVINCE,DISTRICT,RECRUITMENT_TYPE,RECRUITMENT_NUMBER,RECRUITMENT_REQ, REMARK,CREATED_DATE,STATUS,UPDATED_DATE,STATUS_UPDATED_DATE) values(#{city},#{province},#{district},#{recruitment_type},#{recruitment_number}, #{recruitment_req}, #{remark},#{created_date},#{status},#{updated_date},#{status_updated_date})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertDealerRecruitingCity(DealerRecruitingCity recruitingCity);

	@Update(value = {"update VW_DEALER_RECRUITING_CITY set CITY=#{city},PROVINCE=#{province},DISTRICT=#{district},RECRUITMENT_TYPE=#{recruitment_type},RECRUITMENT_NUMBER=#{recruitment_number},RECRUITMENT_REQ=#{recruitment_req}, REMARK=#{remark},CREATED_DATE=#{created_date},STATUS=#{status},UPDATED_DATE=#{updated_date},STATUS_UPDATED_DATE=#{status_updated_date} where ID=#{id}"})
	void updateDealerRecruitingCity(DealerRecruitingCity recruitingCity);

	@Select(value = {"select * from VW_DEALER_RECRUITING_CITY WHERE ID=#{id}"})
	DealerRecruitingCity findDealerRecruitingCityById(long id);

	@Select(value = {"select * from VW_DEALER_RECRUITING_CITY"})
	List<DealerRecruitingCity> findAllDealerRecruitingCities();	
	
	@Select(value = {"select distinct(PROVINCE) from VW_DEALER_RECRUITING_CITY where STATUS='ONLINE'"})
	List<String> findDealerRecruitingProvinces();	
	
	@Select(value = {"select distinct(CITY) from VW_DEALER_RECRUITING_CITY where STATUS='ONLINE' and PROVINCE=#{province}"})
	List<String> findDealerRecruitingCitiesByProvinceName(String province);	
	
	@Select(value = {"select distinct(DISTRICT) from VW_DEALER_RECRUITING_CITY where STATUS='ONLINE' and CITY= #{city}"})
	List<String> findDealerRecruitingDistrictsByCity(String city);	

	@Delete(value = {"delete from VW_DEALER_RECRUITING_CITY where ID=#{id}"})
	void deleteDealerRecruitingCity(long id);	

	@Select(value = {"select count(*) as recordCount from VW_DEALER_RECRUITING_CITY drc "
			+ " WHERE drc.PROVINCE=#{province} and drc.STATUS='ONLINE' and drc.RECRUITMENT_TYPE=#{recruitmentType}"})
	int findTotalCountForDealerRecruitingCitiesByProvince(@Param("province") String province, @Param("recruitmentType") String recruitmentType);	
	
	@Select(value = {"select count(*) as recordCount from VW_DEALER_RECRUITING_CITY drc "
			+ " WHERE drc.CITY=#{city} and drc.STATUS='ONLINE' and drc.RECRUITMENT_TYPE=#{recruitmentType}"})
	int findTotalCountForDealerRecruitingCitiesByCity(@Param("city") String city, @Param("recruitmentType") String recruitmentType);	
	
	@Select(value = {"select count(*) as recordCount from VW_DEALER_RECRUITING_CITY drc "
			+ " WHERE drc.CITY=#{city} and  drc.DISTRICT=#{district} and drc.STATUS='ONLINE' and drc.RECRUITMENT_TYPE=#{recruitmentType} "})
	int findTotalCountForDealerRecruitingCitiesByDistrict(@Param("city") String city,
													@Param("district") String district, 
													@Param("recruitmentType") String recruitmentType);	
		
	@Select(value = {"select * from VW_DEALER_RECRUITING_CITY drc "
			+ " WHERE drc.PROVINCE=#{province} and drc.STATUS='ONLINE' and drc.RECRUITMENT_TYPE=#{recruitmentType} limit #{start},#{end}"})
	List<DealerRecruitingCity> findDealerRecruitingCitiesByProvince(@Param("province") String province, 
																	@Param("recruitmentType") String recruitmentType,
																	@Param("start")  int start, 
									 								@Param("end")  int end);
	
	@Select(value = {"select count(*) as recordCount from VW_DEALER_RECRUITING_CITY drc "
			+ " WHERE  drc.STATUS='ONLINE' and drc.RECRUITMENT_TYPE=#{recruitmentType}"})
	int findTotalCountForDealerRecruitingCities( @Param("recruitmentType") String recruitmentType);	
	
	
    @Select(value = {"select * from VW_DEALER_RECRUITING_CITY drc "
		+ " WHERE drc.STATUS='ONLINE' and drc.RECRUITMENT_TYPE=#{recruitmentType} limit #{start},#{end}"})
        List<DealerRecruitingCity> findDealerRecruitingCities( 
														@Param("recruitmentType") String recruitmentType,
														@Param("start")  int start, 
								 						@Param("end")  int end);
	
		@Select(value = {"select * from VW_DEALER_RECRUITING_CITY drc "
			+ " WHERE drc.CITY=#{city} and drc.STATUS='ONLINE' and drc.RECRUITMENT_TYPE=#{recruitmentType} limit #{start},#{end}"})
	List<DealerRecruitingCity> findDealerRecruitingCitiesByCity(@Param("city") String city, 
																@Param("recruitmentType") String recruitmentType, 
																@Param("start")  int start, 
								 								@Param("end")  int end);
	
	@Select(value = {"select * from VW_DEALER_RECRUITING_CITY drc "
			+ " WHERE drc.CITY=#{city} and  drc.DISTRICT=#{district} and drc.STATUS='ONLINE' and drc.RECRUITMENT_TYPE=#{recruitmentType} limit #{start},#{end}"})
	List<DealerRecruitingCity> findDealerRecruitingCitiesByDistrict(@Param("city") String city, 
											@Param("district") String district, 
			 								@Param("recruitmentType")  String recruitmentType, 
			 								@Param("start")  int start, 
			 								@Param("end")  int end);	
	
	//## Dealer Recruitment
	@Insert(value = {"insert into VW_DEALER_RECRUITMENT "
					+ " (DECLARED_CITY, SELECTED_CITY, SELECTED_PROVINCE, "
					+ " SELECTED_DISTRICT,APPLICANT_NAME, "
					+ " CNT1_NAME, CNT1_POSITION, CNT1_PHONE, CNT1_EMAIL, "
					+ " CNT2_NAME, CNT2_POSITION, CNT2_PHONE, CNT2_EMAIL, "
					+ " REGISTERED_CAPITAL, INDUSTRY_EXPERIENCE, BRAND_OPERATION, "
					+ " MAIN_SITE_ADDRESS, ALTERNATE_SITE_ADDRESS, MAIN_SITE_SOURCE, "
					+ " ALTERNATE_SITE_SOURCE, FORM_STATUS, CREATED_BY, CREATED_DATE, "
					+ " SUBMITTED_DATE, UPDATED_DATE, PROVINCE_CODE, CITY_CODE, DISTRICT_CODE,RECRUITMENT_TYPE,UNIQUE_CODE) "
					+ " values "
					+ " (#{declared_city}, #{selected_city},#{selected_province},#{selected_district},"
					+ " #{applicant_name}, #{cnt1_name}, #{cnt1_position},#{cnt1_phone},#{cnt1_email},"
					+ " #{cnt2_name}, #{cnt2_position},#{cnt2_phone},#{cnt2_email},"
					+ " #{registered_capital},#{industry_experience},#{brand_operation}, "
					+ " #{main_site_address},#{alternate_site_address},#{main_site_source},"
					+ " #{alternate_site_source},#{form_status}, #{created_by}, #{created_date},"
					+ " #{submitted_date}, #{updated_date}, #{province_code}, #{city_code}, #{district_code},#{recruitment_type},#{unique_code})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertDealerRecruitment(DealerRecruitment dealerRecruitment);

	@Update(value = {"update VW_DEALER_RECRUITMENT "
			+ "	set "
			+ " DECLARED_CITY=#{declared_city}, SELECTED_CITY=#{selected_city}, SELECTED_PROVINCE=#{selected_province}, "
			+ " SELECTED_DISTRICT=#{selected_district}, APPLICANT_NAME=#{applicant_name}, CNT1_NAME=#{cnt1_name}, "
			+ " CNT1_POSITION=#{cnt1_position}, CNT1_PHONE=#{cnt1_phone}, CNT1_EMAIL=#{cnt1_email}, CNT2_NAME=#{cnt2_name},"
			+ " CNT2_POSITION=#{cnt2_position}, CNT2_PHONE=#{cnt2_phone}, CNT2_EMAIL=#{cnt2_email}, REGISTERED_CAPITAL=#{registered_capital}, "
			+ " INDUSTRY_EXPERIENCE=#{industry_experience}, BRAND_OPERATION=#{brand_operation}, MAIN_SITE_ADDRESS=#{main_site_address}, "
			+ " ALTERNATE_SITE_ADDRESS=#{alternate_site_address}, MAIN_SITE_SOURCE=#{main_site_source}, ALTERNATE_SITE_SOURCE=#{alternate_site_source}, "
			+ " FORM_STATUS=#{form_status}, CREATED_BY=#{created_by}, CREATED_DATE=#{created_date}, SUBMITTED_DATE=#{submitted_date}, "
			+ " UPDATED_DATE=#{updated_date}, PROVINCE_CODE=#{province_code}, CITY_CODE=#{city_code}, DISTRICT_CODE=#{district_code},"
			+ " RECRUITMENT_TYPE=#{recruitment_type}, UNIQUE_CODE=#{unique_code} "
			+ " where ID=#{id}"})
	void updateDealerRecruitment(DealerRecruitment dealerRecruitment);
	
	
	@Select(value = {"select * from VW_DEALER_RECRUITMENT"})
	List<DealerRecruitment> findAllDealerRecruitments();
	
	@Select(value = {"SELECT count(*) as application_sequence FROM VW_DEALER_RECRUITMENT dr where dr.DECLARED_CITY=#{declaredCityId} and dr.UNIQUE_CODE is not null"})
	int getApplicationSequence(long declaredCityId);
	
	@Select(value = {"select count(*) as totalRecord from VW_DEALER_RECRUITMENT dr WHERE dr.CREATED_BY=#{userId}"})
	int findTotalRecordDealerRecruitmentByUserId(long userId);

	@Select(value = {"select count(*) as totalRecord from VW_DEALER_RECRUITMENT dr WHERE dr.CREATED_BY=#{user_id} and dr.FORM_STATUS=#{form_status}"})
	int findTotalRecordDealerRecruitmentsByStatusAndUserId(@Param("user_id") long userId,@Param("form_status") String status);

	@Select(value = {"select * from VW_DEALER_RECRUITMENT dr WHERE dr.CREATED_BY=#{user_id} limit #{start},#{end}"})
	List<DealerRecruitment> findDealerRecruitmentByUserId(@Param("user_id") long userId,@Param("start")  int start, @Param("end")  int end);
	
	@Select(value = {"select * from VW_DEALER_RECRUITMENT dr WHERE dr.ID=#{id}"})
	DealerRecruitment findDealerRecruitmentById(long id);

	@Select(value = {"select * from VW_DEALER_RECRUITMENT dr WHERE dr.CREATED_BY=#{user_id} and dr.FORM_STATUS=#{form_status} limit #{start},#{end}"})
	List<DealerRecruitment> findDealerRecruitmentsByStatusAndUserId(@Param("user_id") long userId,@Param("form_status") String status,@Param("start")  int start, 
				@Param("end")  int end);	

	@Delete(value = {"delete from VW_DEALER_RECRUITMENT where ID=#{id}"})
	void deleteDealerRecruitment(long id);	

	//## Get Dealer Record list
	@Select(value = {" SELECT d.ID AS id,province.NAME AS province_name,city.NAME AS city_name,district.NAME AS district_name,d.SUBMITTED_DATE AS appln_date,d.FORM_STATUS AS appln_status,dc.REMARK AS remarks"
			+" FROM VW_DEALER_RECRUITMENT d "
			+" LEFT JOIN VW_DEALER_RECRUITING_CITY dc ON dc.ID=d.DECLARED_CITY "
			+" LEFT JOIN VW_CITY city ON city.CODE= dc.CITY "
			+" LEFT JOIN VW_PROVINCE province ON province.CODE=dc.PROVINCE "
			+" LEFT JOIN VW_DISTRICT district ON district.CODE=dc.DISTRICT"})
	List<DealerRecordList> getDealerRecordList();	
	
	@Select(value = {"select * from VW_DEALER WHERE PROVINCE=#{prv_code}"})
	List<Dealer> filterDealerByProvince(String prv_code);
	
	@Select(value = {"select * from VW_DEALER WHERE CITY=#{city_code}"})
	List<Dealer> filterDealerByCity(String city_code);
	
	@Select(value = {"select * from VW_DEALER WHERE DEALER_TYPE_CODE=#{type}"})
	List<Dealer> filterDealerByType(String type);
	
	@Select(value = {"select * from VW_DEALER WHERE PROVINCE=#{province} and CITY=#{city} and DEALER_TYPE_CODE=#{dealer_type_code}"})
	List<Dealer> filterDealerByCityProvinceAndType(Dealer dealer);

	@Select(value = {"select * from VW_DEALER WHERE PROVINCE=#{province} and CITY=#{city}"})
	List<Dealer> filterDealerByCityProvince(String province,String city);
	
	@Select(value={"SELECT d.id, d.city AS city_code, c.name AS city_name, d.province AS province_code, p.name AS province_name, d.name, d.full_name, d.address, d.dealer_type_code, d.created_date, d.update_date, d.lattitude, d.longitude, d.website_url, d.promotion_image_url, d.promotion_page_url, d.sales_hotline, d.customer_hotline, d.after_sales, d.postal_code FROM VW_DEALER d JOIN VW_PROVINCE p ON p.CODE=d.province JOIN VW_CITY c ON c.CODE=d.city WHERE p.LANG_CODE=#{lang_code} AND c.LANG_CODE=#{lang_code}"})
	List<DealerCityProvince> getDealerCityProvinceName (String lang_code);
}