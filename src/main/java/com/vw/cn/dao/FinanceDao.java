package com.vw.cn.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.customDomain.FinancialLoan;
import com.vw.cn.domain.FinancialInstitution;
import com.vw.cn.domain.FinancialLoanCalculation;
import com.vw.cn.domain.FinancialProducts;
import com.vw.cn.domain.Loan;
import com.vw.cn.domain.ModelFinancialInterest;
/**
 * @author karthikeyan_v
 */
@Mapper
public interface FinanceDao {

	//## FinancialInstitution
	@Insert(value = {"insert into VW_FINANCIAL_INSTITUTION(CODE,BANK_NAME,BRANCH_NAME,CITY_CODE,PROVINCE_CODE,DISTRICT_CODE,DELETE_FLAG,LANG,LAST_UPDATED_DATE) values(#{code},#{bank_name},#{branch_name},#{city_code},#{province_code},#{district_code},#{delete_flag},#{lang},#{last_update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertFinancialInstitution(FinancialInstitution financialInstitution);

	@Update(value = {"update VW_FINANCIAL_INSTITUTION set CODE=#{code},BANK_NAME=#{bank_name},BRANCH_NAME=#{branch_name},CITY_CODE=#{city_code},PROVINCE_CODE=#{province_code},DISTRICT_CODE=#{district_code},DELETE_FLAG=#{delete_flag},LANG=#{lang},LAST_UPDATED_DATE=#{last_update_time} where ID=#{id}"})
	void updateFinancialInstitution(FinancialInstitution financialInstitution); 

	@Select(value = {"select * from VW_FINANCIAL_INSTITUTION WHERE ID=#{id} and DELETE_FLAG=0"})
	FinancialInstitution findFinancialInstitutionById(long id);			

	@Select(value = {"select * from VW_FINANCIAL_INSTITUTION WHERE DELETE_FLAG=0 and LANG=#{lang}"})
	List<FinancialInstitution> findAllFinancialInstitutions(String lang);	

	//## FinancialProducts
	@Insert(value = {"insert into VW_FINANCIAL_PRODUCTS(CODE,FINANCIAL_INSTITUTION,NAME,DESCRIPTION,LANG,DELETE_FLAG,LAST_UPDATED_DATE) values(#{code},#{financial_institution},#{name},#{description},#{lang},#{delete_flag},#{last_updated_date})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertFinancialProducts(FinancialProducts financialProducts);

	@Update(value = {"update VW_FINANCIAL_PRODUCTS set CODE=#{code},FINANCIAL_INSTITUTION=#{financial_institution},NAME=#{name},DESCRIPTION=#{description},LANG=#{lang},DELETE_FLAG=#{delete_flag},LAST_UPDATED_DATE=#{last_updated_date} where ID=#{id}"})
	void updateFinancialProducts(FinancialProducts financialProducts); 

	@Select(value = {"select * from VW_FINANCIAL_PRODUCTS WHERE ID=#{id} and DELETE_FLAG=0"})
	FinancialProducts findFinancialProductsById(long id);			

	@Select(value = {"select * from VW_FINANCIAL_PRODUCTS WHERE DELETE_FLAG=0 and LANG=#{lang}"})
	List<FinancialProducts> findAllFinancialProducts(String lang);	

	//##ModelFinancialInterest
	@Insert(value = {"insert into VW_MODEL_FINANCIAL_INTEREST(MODEL_CODE,FINANCIAL_PRODUCT_CODE,INTEREST_RATE,CUSTOMER_INTEREST_RATE) values(#{model_code},#{financial_product_code},#{interest_rate},#{customer_interest_rate})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertModelFinancialInterest(ModelFinancialInterest modelFinancialInterest);

	@Update(value = {"update VW_MODEL_FINANCIAL_INTEREST set FINANCIAL_PRODUCT_CODE=#{financial_product_code},INTEREST_RATE=#{interest_rate},CUSTOMER_INTEREST_RATE=#{customer_interest_rate} where MODEL_CODE=#{model_code}"})
	void updateModelFinancialInterest(ModelFinancialInterest modelFinancialInterest); 

	@Select(value = {"select * from VW_MODEL_FINANCIAL_INTEREST WHERE MODEL_CODE=#{model_code}"})
	ModelFinancialInterest findModelFinancialInterestByCode(String model_code);			

	@Select(value = {"select * from VW_MODEL_FINANCIAL_INTEREST"})
	List<ModelFinancialInterest> findAllModelFinancialInterests();	

	//## Loan
	@Insert(value={"insert into VW_LOAN(CAR_SERIES,CAR_MODEL,FINANCIAL_INSTITUTIONS,FINANCIAL_PRODUCT,TOTAL_PAYMENT,DOWN_PAYMENT,"
			+ "LOAN_TENURE,INTEREST,DEALER_PROVINCE,DEALER_CITY,DEALER,NAME,TITLE,PHONE,EXPECTED_PURCHASE_TIME,LOAN_STATUS,"
			+ "DELETE_FLAG,LAST_UPDATED_DATE,APPLICATION_DATE,CREATED_BY) values(#{car_series},#{car_model},#{financial_institutions},#{financial_product},"
			+ "#{total_payment},#{down_payment},#{loan_tenure},#{interest},#{dealer_province},#{dealer_city},#{dealer},#{name},#{title},"
			+ "#{phone},#{expected_purchase_time},#{loan_status},#{delete_flag},#{last_updated_date},#{application_date},#{created_by})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertLoanRecord(Loan loan);

	@Update(value={"update VW_LOAN set CAR_SERIES=#{car_series},CAR_MODEL=#{car_model},FINANCIAL_INSTITUTIONS=#{financial_institutions},"
			+ "FINANCIAL_PRODUCT=#{financial_product},TOTAL_PAYMENT=#{total_payment},DOWN_PAYMENT=#{down_payment},LOAN_TENURE=#{loan_tenure},"
			+ "INTEREST=#{interest},DEALER_PROVINCE=#{dealer_province},DEALER_CITY=#{dealer_city},DEALER=#{dealer},NAME=#{name},TITLE=#{title},"
			+ "PHONE=#{phone},EXPECTED_PURCHASE_TIME=#{expected_purchase_time},LOAN_STATUS=#{loan_status},DELETE_FLAG=#{delete_flag},"
			+ "LAST_UPDATED_DATE=#{last_updated_date},APPLICATION_DATE=#{application_date},CREATED_BY=#{created_by} where ID=#{id}"})
	void updateLoanRecord(Loan loan);

	@Select(value={"select * from VW_LOAN where ID=#{id} and DELETE_FLAG=0"})
	public Loan getLoanRecordById(long id);

	@Select(value={"select * from VW_LOAN where DELETE_FLAG=0"})
	public List<Loan> getAllLoanRecords();	

	@Select(value={"select * from VW_MODEL_FINANCIAL_INTEREST where MODEL_CODE=#{car_model} and FINANCIAL_PRODUCT_CODE=#{financial_product}"})
	ModelFinancialInterest findModelFinancialInterestByModelCodeAndFinancialProduct(Loan loan);
	
	// ############ Financial Loan Calculation ##############
	@Select(value={"SELECT * FROM VW_FINANCIAL_LOAN_CALCULATION flc "
			+ " where flc.MODAL_CODE=#{car_model} and flc.FINANCIAL_PRODUCT_CODE=#{financial_product} and "
			+ " flc.RR_PRICE=#{total_payment} and flc.DOWN_PAYMENT_RATE=#{down_payment} and flc.LOAN_TENURE=#{loan_tenure}"})
	FinancialLoanCalculation getFinancialLoanCalculations(Loan loan);
	
	/*@Select(value={"SELECT * FROM VW_FINANCIAL_LOAN_CALCULATION flc "
			+ " where flc.MODAL_CODE=#{car_model} and "
			+ " flc.RR_PRICE=#{total_payment} and flc.DOWN_PAYMENT_RATE=#{down_payment} and flc.LOAN_TENURE=#{loan_tenure}"})*/
	@Select(value={"SELECT flc.ID AS id, flc.MODAL_CODE AS modal_Code,mo.NAME AS model_name,ser.CODE AS series_code,ser.NAME AS series_name, flc.FINANCIAL_PRODUCT_CODE AS financial_product_code, "
			+ "fp.NAME AS financial_product_name,fi.CODE AS financial_Institution_code,fi.BANK_NAME AS financial_institution_name,flc.RR_PRICE AS rr_price, flc.DOWN_PAYMENT_RATE AS down_payment_rate, "
			+ "flc.LOAN_TENURE AS loan_tenure, flc.DOWN_PAYMENT_AMOUNT AS down_payment_amount, flc.LOAN_AMOUNT AS loan_amount, flc.MONTHLY_INSTALLMENT AS monthly_installment, "
			+ "flc.FEE_RATE AS fee_rate, flc.CUSTOMER_RATE AS customer_rate, flc.CREATED_DATE AS created_date, flc.INTEREST_AMOUNT AS interest_amount FROM VW_FINANCIAL_LOAN_CALCULATION flc "
			+ "JOIN vw_model mo ON flc.modal_Code = mo.CODE JOIN vw_financial_product fp ON flc.financial_product_code = fp.CODE "
			+ "JOIN vw_financial_institution fi ON fp.FINANCIAL_INSTITUTION= fi.CODE JOIN vw_series ser ON mo.SERIES_CODE=ser.CODE "
			+ "WHERE flc.MODAL_CODE=#{car_model} and flc.RR_PRICE=#{total_payment} and flc.DOWN_PAYMENT_RATE=#{down_payment} and "
			+ "flc.LOAN_TENURE=#{loan_tenure} GROUP BY flc.ID;"})
	List<FinancialLoan> getFinancialLoanCalculation(Loan loan);
	
	@Select(value = {"select * from VW_FINANCIAL_PRODUCT WHERE FINANCIAL_INSTITUTION=#{financial_institution} AND DELETE_FLAG=0 AND LANG=#{lang}"})
	 List<FinancialProducts> findAllFinancialProductsByInstitution(FinancialProducts financialproducts);
	
}
