package com.vw.cn.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.domain.GenericDetails;

/**
 * @author Harihara Subramanian
 */
@Mapper
public interface GenericDetailsDao {

	@Insert(value={"insert into VW_GENERIC_DETAIL(NAME,DISCRIMINATOR,DESCRIPTION,VALUE,EXTRAFIELD) values(#{name},#{discriminator},#{description},#{value},#{extrafield})"})	
	@Options(useGeneratedKeys = true, keyProperty = "id")
	public void insertGenericInfo(GenericDetails genericDetails);

	@Update(value={"update VW_GENERIC_DETAIL set NAME=#{name},DISCRIMINATOR=#{discriminator},DESCRIPTION=#{description},VALUE=#{value},EXTRAFIELD=#{extrafield} where ID=#{id} "})
	public void updategenericDetails(GenericDetails genericDetails);

	@Select(value={"select * from VW_GENERIC_DETAIL where DISCRIMINATOR=#{discriminator}"})
	public List<GenericDetails> getGenericDetails(String discriminator);
}
