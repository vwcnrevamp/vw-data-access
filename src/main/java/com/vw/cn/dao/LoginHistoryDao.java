package com.vw.cn.dao;
/**
 * @author karthikeyan_v
 */
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.domain.LoginHistory;

@Mapper
public interface LoginHistoryDao
{
	@Insert(value = {"insert into VW_LOGIN_HISTORY(USER_ID,TOKEN,LOGIN_TIME,LOGOUT_TIME) values(#{user_id},#{token},#{login_time},#{logout_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertLoginHistory(LoginHistory loginHistory);

	@Update(value = {"update VW_LOGIN_HISTORY set USER_ID=#{user_id},TOKEN=#{token},LOGIN_TIME=#{login_time},LOGOUT_TIME=#{logout_time} where ID=#{id}"})
	void updateLoginHistory(LoginHistory loginHistory);

	@Select(value = {"select * from VW_LOGIN_HISTORY WHERE ID=#{id}"})
	LoginHistory findLoginHistoryById(long id);

	@Select(value = {"select * from VW_LOGIN_HISTORY WHERE USER_ID=#{user_id}"})
	LoginHistory findLoginHistoryByUserID(long id);
	
	@Select(value = {"select * from VW_LOGIN_HISTORY WHERE TOKEN=#{token}"})
	LoginHistory findLoginHistoryByToken(String token);

	@Select(value = {"select * from VW_LOGIN_HISTORY"})
	List<LoginHistory> findAllLoginHistorys();	

	@Delete(value = {"delete from VW_LOGIN_HISTORY where ID=#{id}"})
	void deleteLoginHistory(long id);
}
