package com.vw.cn.dao;
/**
 * @author karthikeyan_v
 */
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.customDomain.DistrictCityProvince;
import com.vw.cn.customDomain.ProvinceAndCity;
import com.vw.cn.domain.CarParameter;
import com.vw.cn.domain.CarParameterType;
import com.vw.cn.domain.CarSeries;
import com.vw.cn.domain.City;
import com.vw.cn.domain.Currency;
import com.vw.cn.domain.DealerType;
import com.vw.cn.domain.District;
import com.vw.cn.domain.Language;
import com.vw.cn.domain.Province;
import com.vw.cn.domain.RecruitmentNetworkType;
import com.vw.cn.domain.Role;
import com.vw.cn.domain.SourceSite;

@Mapper
public interface MasterDataDao
{
	//##Role

	@Insert(value = {"insert into VW_ROLE(NAME,DESCRIPTION,DELETE_FLAG,UPDATE_TIME) values(#{name},#{description},#{delete_flag},#{update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertRole(Role role);

	@Update(value = {"update VW_ROLE set NAME=#{name},DESCRIPTION=#{description},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateRole(Role role);

	@Select(value = {"select * from VW_ROLE WHERE ID=#{id}"})
	Role findRoleById(long id);

	@Select(value = {"select * from VW_ROLE"})
	List<Role> findAllRoles();	

	@Delete(value = {"delete from VW_ROLE where ID=#{id}"})
	void deleteRole(long id);

	//##Lang

	@Insert(value = {"insert into VW_LANG(CODE,NAME,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{delete_flag},#{update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertLanguage(Language lang);

	@Update(value = {"update VW_LANG set CODE=#{code},NAME=#{name},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateLanguage(Language lang);

	@Select(value = {"select * from VW_LANG WHERE ID=#{id}"})
	Language findLanguageById(long id);

	@Select(value = {"select * from VW_LANG WHERE CODE=#{code}"})
	Language findLanguageByCode(String code);

	@Select(value = {"select * from VW_LANG"})
	List<Language> findAllLanguages();	

	@Delete(value = {"delete from VW_LANG where ID=#{id}"})
	void deleteLanguage(long id);

	//##Province

	@Insert(value = {"insert into VW_PROVINCE(CODE,NAME,LANG_CODE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{lang_code},#{delete_flag},#{update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertProvince(Province province);

	@Update(value = {"update VW_PROVINCE set CODE=#{code},NAME=#{name},LANG_CODE=#{lang_code},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateProvince(Province province);

	@Select(value = {"select p.ID,p.NAME,p.LANG_CODE,p.CODE,l.ID as LANG_ID,l.NAME as LANG_NAME from VW_PROVINCE p left outer join VW_LANG l on p.LANG_CODE=l.ID WHERE p.ID=#{id}"})
	Province findProvinceById(long id);

	@Select(value = {"select * from VW_PROVINCE WHERE CODE=#{code} and LANG_CODE=#{lang_code}"})
	Province findProvinceByCode(String code,String lang_code);

	@Select(value = {"select * from VW_PROVINCE where DELETE_FLAG=0 and LANG_CODE=#{lang_code}"})
	List<Province> findAllProvinces(String lang_code);	

	@Delete(value = {"delete from VW_PROVINCE where ID=#{id}"})
	void deleteProvince(long id);

	//##City

	@Insert(value = {"insert into VW_CITY(CODE,NAME,PROVINCE_CODE,LANG_CODE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{province_code},#{lang_code},#{delete_flag},#{update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertCity(City city);

	@Update(value = {"update VW_CITY set CODE=#{code},NAME=#{name},PROVINCE_CODE=#{province_code},LANG_CODE=#{lang_code},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateCity(City city);

	@Select(value = {"select * from VW_CITY WHERE ID=#{id}"})
	City findCityById(long id);

	@Select(value = {"select * from VW_CITY WHERE CODE=#{code} and LANG_CODE=#{lang_code}"})
	City findCityByCode(String code,String lang_code);
	
	@Select(value = {"select * from VW_CITY WHERE NAME=#{name}"})
	City findCityByName(String name);

	@Select(value = {"select * from VW_CITY where LANG_CODE=#{lang_code}"})
	List<City> findAllCities(String lang_code);	

	@Delete(value = {"delete from VW_CITY where ID=#{id}"})
	void deleteCity(long id);

	//##District

	@Insert(value = {"insert into VW_DISTRICT(CODE,NAME,CITY_CODE,LANG_CODE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{city_code},#{lang_code},#{delete_flag},#{update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertDistrict(District district);

	@Update(value = {"update VW_DISTRICT set CODE=#{code},NAME=#{name},CITY_CODE=#{city_code},LANG_CODE=#{lang_code},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateDistrict(District district);

	@Select(value = {"select * from VW_DISTRICT WHERE ID=#{id}"})
	District findDistrictById(long id);

	@Select(value = {"select * from VW_DISTRICT WHERE CODE=#{code} and LANG_CODE=#{lang_code}"})
	District findDistrictByCode(String code,String lang_code);

	@Select(value = {"select * from VW_DISTRICT where LANG_CODE=#{lang_code}"})
	List<District> findAllDistricts(String lang_code);	

	@Delete(value = {"delete from VW_DISTRICT where ID=#{id}"})
	void deleteDistrict(long id);

	//##RecruitmentNetworkType

	@Insert(value = {"insert into VW_RECRUITMENT_NETWORK_TYPE(CODE,NAME,LANG_CODE,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{lang_code},#{delete_flag},#{update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertRecruitmentNetworkType(RecruitmentNetworkType recruitmentNetwork);

	@Update(value = {"update VW_RECRUITMENT_NETWORK_TYPE set CODE=#{code},NAME=#{name},LANG_CODE=#{lang_code},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateRecruitmentNetworkType(RecruitmentNetworkType recruitmentNetwork);

	@Select(value = {"select * from VW_RECRUITMENT_NETWORK_TYPE WHERE ID=#{id}"})
	RecruitmentNetworkType findRecruitmentNetworkTypeById(long id);

	@Select(value = {"select * from VW_RECRUITMENT_NETWORK_TYPE WHERE CODE=#{code} and LANG_CODE=#{lang_code}"})
	RecruitmentNetworkType findRecruitmentNetworkTypeByCode(String code,String lang_code);

	@Select(value = {"select * from VW_RECRUITMENT_NETWORK_TYPE  where LANG_CODE=#{lang_code}"})
	List<RecruitmentNetworkType> findAllRecruitmentNetworkTypes(String lang_code);	

	@Delete(value = {"delete from VW_RECRUITMENT_NETWORK_TYPE where ID=#{id}"})
	void deleteRecruitmentNetworkType(long id);	

	//##Currency

	@Insert(value = {"insert into VW_CURRENCY(CODE,NAME,SYMBOL,DELETE_FLAG,UPDATE_TIME) values(#{code},#{name},#{symbol},#{delete_flag},#{update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertCurrency(Currency currency);

	@Update(value = {"update VW_CURRENCY set CODE=#{code},NAME=#{name},SYMBOL=#{symbol},DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateCurrency(Currency currency);

	@Select(value = {"select * from VW_CURRENCY WHERE ID=#{id}"})
	Currency findCurrencyById(long id);

	@Select(value = {"select * from VW_CURRENCY"})
	List<Currency> findAllCurrencys();	

	@Delete(value = {"delete from VW_CURRENCY where ID=#{id}"})
	void deleteCurrency(long id);

	//##Get Lists
	@Select(value={"SELECT c.id as city_id, p.ID as province_id, c.CODE as city_code, c.LANG_CODE as lang_code, c.NAME as city_name, "
			+ " c.LONGITUDE as longitude, c.LATITUDE as latitude, p.CODE as province_code, p.NAME  as province_name "
			+ " FROM VW_CITY c inner JOIN VW_PROVINCE p on c.PROVINCE_CODE=p.CODE and "
			+ " c.DELETE_FLAG=0 and p.DELETE_FLAG=0 "
			+ " where p.LANG_CODE=#{lang_code} and c.LANG_CODE=#{lang_code} order by p.ID"})
	List<ProvinceAndCity> getallprovinceAndCities(String lang_code);

	@Select(value={"SELECT d.code as district_code,d.name as district_name,c.code as city_code,c.name as city_name,p.code as province_code,p.name as province_name "
			+ "FROM VW_DISTRICT d INNER JOIN VW_CITY c on d.city_code=c.code INNER JOIN VW_PROVINCE p on c.province_code=p.code"
			+ " where p.LANG_CODE=#{lang_code} AND c.LANG_CODE=#{lang_code} AND d.LANG_CODE=#{lang_code} order by d.ID"})
	List<DistrictCityProvince> getAllDistrictCityProvince(String lang_code);

	//## SourceSite
	@Insert(value = {"insert into VW_SOURCE_SITE(CODE,NAME,LANG_CODE) values(#{code},#{name},#{lang_code})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertSourceSite(SourceSite sourceSite);

	@Update(value = {"update VW_SOURCE_SITE set CODE=#{code},NAME=#{name},LANG_CODE=#{lang_code} where ID=#{id}"})
	void updateSourceSite(SourceSite sourceSite);

	@Select(value = {"select * from VW_SOURCE_SITE WHERE ID=#{id}"})
	SourceSite findSourceSiteById(long id);

	@Select(value = {"select * from VW_SOURCE_SITE WHERE CODE=#{code} and LANG_CODE=#{lang_code}"})
	SourceSite findSourceSiteByCode(String code,String lang_code);

	@Select(value = {"select * from VW_SOURCE_SITE where LANG_CODE=#{lang_code}"})
	List<SourceSite> findAllSourceSites(String lang_code);	

	@Delete(value = {"delete from VW_SOURCE_SITE where ID=#{id}"})
	void deleteSourceSite(long id);
	
	//## Dealer Type
	@Insert(value = {"insert into VW_DEALER_TYPE(CODE,NAME,LANG_CODE) values(#{code},#{name},#{lang_code})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertDealerType(DealerType dealerType);

	@Update(value = {"update VW_DEALER_TYPE set CODE=#{code},NAME=#{name},LANG_CODE=#{lang_code} where ID=#{id}"})
	void updateDealerType(DealerType dealerType);

	@Select(value = {"select * from VW_DEALER_TYPE WHERE ID=#{id}"})
	DealerType findDealerTypeById(long id);

	@Select(value = {"select * from VW_DEALER_TYPE where LANG_CODE=#{lang_code}"})
	List<DealerType> findAllDealerTypes(String lang_code);	

	@Delete(value = {"delete from VW_DEALER_TYPE where ID=#{id}"})
	void deleteDealerType(long id);	
	
	@Select(value={"SELECT * FROM VW_DISTRICT  where CITY_CODE=#{city_code} and LANG_CODE=#{lang_code}"})
	List<District> findDistrictByCity(@Param("city_code") String citycode, @Param("lang_code") String lang);
	
	
}