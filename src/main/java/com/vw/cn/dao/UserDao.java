package com.vw.cn.dao;
/**
 * @author karthikeyan_v
 */
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.vw.cn.domain.User;
import com.vw.cn.domain.UserDetail;
import com.vw.cn.domain.UserRole;

@Mapper
public interface UserDao
{
	@Insert(value = {"insert into VW_USER(ACCOUNT,PASSWORD,ACCOUNT_TYPE,CREATED_DATETIME,LAST_LOGIN_DATETIME,IS_ENABLED,LAST_MODIFIED_DATETIME,DELETE_FLAG) values(#{account},#{password},#{account_type},#{created_date},#{last_login_date},#{is_enabled},#{last_modified_date},#{delete_flag})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertUser(User user);

	@Update(value = {"update VW_USER set ACCOUNT=#{account},PASSWORD=#{password},ACCOUNT_TYPE=#{account_type},CREATED_DATETIME=#{created_date},LAST_LOGIN_DATETIME=#{last_login_date},IS_ENABLED=#{is_enabled},LAST_MODIFIED_DATETIME=#{last_modified_date} where ID=#{id}"})
	void updateUser(User user);

	@Select(value = {"select * from VW_USER WHERE ID=#{id} and IS_ENABLED=1 and DELETE_FLAG=0"})
	User findUserById(long id);

	@Select(value = {"select * from VW_USER WHERE ACCOUNT=#{account} and IS_ENABLED=1 and DELETE_FLAG=0"})
	User findUserByAccount(String account);	

	@Select(value = {"select * from VW_USER WHERE IS_ENABLED=1 and DELETE_FLAG=0"})
	List<User> findAllUsers();	

	//@Delete(value = {"delete from VW_USER where ID=#{id}"})
	//void deleteUser(long id);

	@Update(value = {"update VW_USER set LAST_MODIFIED_DATETIME=#{last_modified_date},DELETE_FLAG=#{delete_flag} where ID=#{id}"})
	void deleteUser(User user);

	//## User details

	@Insert(value = {"insert into VW_USER_DETAIL(FIRST_NAME,LAST_NAME,EMAIL,PHONE,ADDRESS,CITY,USER_ID,CREATED_DATETIME,LAST_MODIFIED_DATETIME,DELETE_FLAG) values(#{first_name},#{last_name},#{email},#{phone},#{address},#{city},#{user_id},#{created_date},#{last_modified_date},#{delete_flag})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertUserDetail(UserDetail userDetail);

	@Update(value = {"update VW_USER_DETAIL set FIRST_NAME=#{first_name},LAST_NAME=#{last_name},EMAIL=#{email},PHONE=#{phone},ADDRESS=#{address},CITY=#{city},USER_ID=#{user_id},CREATED_DATETIME=#{created_date},LAST_MODIFIED_DATETIME=#{last_modified_date} where ID=#{id}"})
	void updateUserDetail(UserDetail userDetail);

	@Select(value = {"select * from VW_USER_DETAIL WHERE ID=#{id} and DELETE_FLAG=0"})
	UserDetail findUserDetailById(long id);	

	@Select(value = {"select * from VW_USER_DETAIL and DELETE_FLAG=0"})
	List<UserDetail> findAllUserDetails();	

	//@Delete(value = {"delete from VW_USER_DETAIL where ID=#{id}"})
	@Update(value = {"update VW_USER_DETAIL set LAST_MODIFIED_DATETIME=#{last_modified_date},DELETE_FLAG=#{delete_flag} where ID=#{id}"})
	void deleteUserDetail(UserDetail userDetail);

	//##User Role

	@Insert(value = {"insert into VW_USER_ROLE(USER_ID,ROLE_ID,DELETE_FLAG,UPDATE_TIME) values(#{user_id},#{role_id},#{delete_flag},#{update_time})"})
	@Options(useGeneratedKeys = true, keyProperty = "id")
	void insertUserRole(UserRole userRole);

	@Update(value = {"update VW_USER_ROLE set USER_ID=#{user_id},ROLE_ID=#{role_id},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void updateUserRole(UserRole userRole);

	@Select(value = {"select * from VW_USER_ROLE WHERE ID=#{id} and DELETE_FLAG=0"})
	UserRole findUserRoleById(long id);		

	@Select(value = {"select * from VW_USER_ROLE WHERE USER_ID=#{user_id} and DELETE_FLAG=0"})
	UserRole findUserRoleByUserId(long user_id);		

	@Select(value = {"select * from VW_USER_ROLE WHERE ROLE_ID=#{role_id} and DELETE_FLAG=0"})
	UserRole findUserRoleByRoleId(long role_id);		

	@Select(value = {"select * from VW_USER_ROLE and DELETE_FLAG=0"})
	List<UserRole> findAllUserRoles();	

	//@Delete(value = {"delete from VW_USER_ROLE where ID=#{id}"})
	@Update(value = {"update VW_USER_ROLE set DELETE_FLAG=#{delete_flag},UPDATE_TIME=#{update_time} where ID=#{id}"})
	void deleteUserRole(UserRole userRole);
}
