package com.vw.cn.domain;
/**
 * @author Harihara Subramanian
 */
import java.util.Date;

public class CarModel {

	private long id;

	private String code;

	private String name;

	private String series_code;

	private String lang;

	private String currency_code;

	private float recommended_retail_price;

	private Float price;

	private char delete_flag;

	private Date update_time;

	private String image_url;
	
	private String url;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}	

	public void setName(String model_name) {
		this.name = model_name;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Date getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSeries_code() {
		return series_code;
	}

	public void setSeries_code(String series_code) {
		this.series_code = series_code;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	public char getDelete_flag() {
		return delete_flag;
	}

	public void setDelete_flag(char delete_flag){
		this.delete_flag=delete_flag;
	}
	public float getRecommended_retail_price() {
		return recommended_retail_price;
	}

	public void setRecommended_retail_price(float recommended_retail_price) {
		this.recommended_retail_price = recommended_retail_price;
	}

	public String getName() {
		return name;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}	
	
}