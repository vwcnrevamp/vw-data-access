package com.vw.cn.domain;

public class CarModelParameter {
	private long id;
	private String value;
	private String imageUrl;
	private String lang;
	private String category;
	private boolean comparable;
	private boolean displayImage;
	private String model_code;
	private String param_code;
	private String param_type;
	private boolean show_circle;

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public boolean isComparable() {
		return this.comparable;
	}

	public void setComparable(boolean comparable) {
		this.comparable = comparable;
	}

	public boolean isDisplayImage() {
		return this.displayImage;
	}

	public void setDisplayImage(boolean displayImage) {
		this.displayImage = displayImage;
	}

	public String getModel_code() {
		return model_code;
	}

	public void setModel_code(String model_code) {
		this.model_code = model_code;
	}

	public String getParam_code() {
		return param_code;
	}

	public void setParam_code(String param_code) {
		this.param_code = param_code;
	}

	public String getParam_type() {
		return param_type;
	}

	public void setParam_type(String param_type) {
		this.param_type = param_type;
	}

	public boolean isShow_circle() {
		return show_circle;
	}

	public void setShow_circle(boolean show_circle) {
		this.show_circle = show_circle;
	}

	

}