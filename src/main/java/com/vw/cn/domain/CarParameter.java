package com.vw.cn.domain;

public class CarParameter
{
  private long id;
  private String code;
  private String name;
  private String lang;
  private String description;
  private String carParameterType;
  private boolean displayCircle;

  public long getId()
  {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLang() {
    return this.lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getCarParameterType() {
    return this.carParameterType;
  }

  public void setCarParameterType(String carParameterType) {
    this.carParameterType = carParameterType;
  }

	public boolean isDisplayCircle() {
		return displayCircle;
	}
	
	public void setDisplayCircle(boolean displayCircle) {
		this.displayCircle = displayCircle;
	}


}