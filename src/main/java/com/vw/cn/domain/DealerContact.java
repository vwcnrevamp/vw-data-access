package com.vw.cn.domain;

import java.util.Date;

/**
 * @author karthikeyan_v
 */
public class DealerContact {

	private long id;

	private String name;

	private String phone;

	private String email;

	private String job_title;

	private String city;

	private long last_modified_by;

	private long dealer_id;

	private Date created_date;

	private Date last_modified_date;

	private long contact_type;

	//private User user;

	//private DealerRegistration dealerRegistration;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getJob_title() {
		return job_title;
	}

	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getLast_modified_by() {
		return last_modified_by;
	}

	public void setLast_modified_by(long last_modified_by) {
		this.last_modified_by = last_modified_by;
	}

	public long getDealer_id() {
		return dealer_id;
	}

	public void setDealer_id(long dealer_id) {
		this.dealer_id = dealer_id;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getLast_modified_date() {
		return last_modified_date;
	}

	public void setLast_modified_date(Date last_modified_date) {
		this.last_modified_date = last_modified_date;
	}

	public long getContact_type() {
		return contact_type;
	}

	public void setContact_type(long contact_type) {
		this.contact_type = contact_type;
	}	
}