package com.vw.cn.domain;

import java.util.Date;
/**
 * @author karthikeyan_v
 */
public class DealerRecruitingCity {

	private long id;

	private String city;

	private String province;

	private String district;

	private String recruitment_type;

	private long recruitment_number;

	private String remark;

	private Date created_date;

	private String status;

	private Date updated_date;

	private Date status_updated_date;
	
	private String recruitment_req;
	
	private String city_code;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getRecruitment_type() {
		return recruitment_type;
	}

	public void setRecruitment_type(String recruitment_type) {
		this.recruitment_type = recruitment_type;
	}

	public long getRecruitment_number() {
		return recruitment_number;
	}

	public void setRecruitment_number(long recruitment_number) {
		this.recruitment_number = recruitment_number;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	public Date getStatus_updated_date() {
		return status_updated_date;
	}

	public void setStatus_updated_date(Date status_updated_date) {
		this.status_updated_date = status_updated_date;
	}

	public String getRecruitment_req() {
		return recruitment_req;
	}

	public void setRecruitment_reg(String recruitment_req) {
		this.recruitment_req = recruitment_req;
	}

	public String getCity_code() {
		return city_code;
	}

	public void setCity_code(String city_code) {
		this.city_code = city_code;
	}

}