package com.vw.cn.domain;

import java.util.Date;

/**
 * @author karthikeyan_v
 */
public class DealerRecruitment {

	private long id;

	private long declared_city;

	private String selected_city;
	
	private String selected_province;
	
	private String selected_district;
	
	private String applicant_name;
	
	private String cnt1_name;
	
	private String cnt1_position;
	
	private String cnt1_phone;
	
	private String cnt1_email;
	
	private String cnt2_name;
	
	private String cnt2_position;
	
	private String cnt2_phone;
	
	private String cnt2_email;
	
	private String registered_capital;

	private long industry_experience;

	private String brand_operation;

	private String main_site_address;
	
	private String alternate_site_address;

	private String main_site_source;	

	private String alternate_site_source;
	
	private String form_status;

	private Date created_date;
	
	private long created_by;

	private Date submitted_date;

	private Date updated_date;	
	
	private String province_code;
	
	private String city_code;
	
	private String district_code;
	
	private String recruitment_type;
	
	private String unique_code;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDeclared_city() {
		return declared_city;
	}

	public void setDeclared_city(long declared_city) {
		this.declared_city = declared_city;
	}

	public String getSelected_city() {
		return selected_city;
	}

	public void setSelected_city(String selected_city) {
		this.selected_city = selected_city;
	}

	public String getSelected_province() {
		return selected_province;
	}

	public void setSelected_province(String selected_province) {
		this.selected_province = selected_province;
	}

	public String getSelected_district() {
		return selected_district;
	}

	public void setSelected_district(String selected_district) {
		this.selected_district = selected_district;
	}

	public String getApplicant_name() {
		return applicant_name;
	}

	public void setApplicant_name(String applicant_name) {
		this.applicant_name = applicant_name;
	}

	public String getCnt1_name() {
		return cnt1_name;
	}

	public void setCnt1_name(String cnt1_name) {
		this.cnt1_name = cnt1_name;
	}

	public String getCnt1_position() {
		return cnt1_position;
	}

	public void setCnt1_position(String cnt1_position) {
		this.cnt1_position = cnt1_position;
	}

	public String getCnt1_phone() {
		return cnt1_phone;
	}

	public void setCnt1_phone(String cnt1_phone) {
		this.cnt1_phone = cnt1_phone;
	}

	public String getCnt1_email() {
		return cnt1_email;
	}

	public void setCnt1_email(String cnt1_email) {
		this.cnt1_email = cnt1_email;
	}

	public String getCnt2_name() {
		return cnt2_name;
	}

	public void setCnt2_name(String cnt2_name) {
		this.cnt2_name = cnt2_name;
	}

	public String getCnt2_position() {
		return cnt2_position;
	}

	public void setCnt2_position(String cnt2_position) {
		this.cnt2_position = cnt2_position;
	}

	public String getCnt2_phone() {
		return cnt2_phone;
	}

	public void setCnt2_phone(String cnt2_phone) {
		this.cnt2_phone = cnt2_phone;
	}

	public String getCnt2_email() {
		return cnt2_email;
	}

	public void setCnt2_email(String cnt2_email) {
		this.cnt2_email = cnt2_email;
	}

	public String getRegistered_capital() {
		return registered_capital;
	}

	public void setRegistered_capital(String registered_capital) {
		this.registered_capital = registered_capital;
	}

	public long getIndustry_experience() {
		return industry_experience;
	}

	public void setIndustry_experience(long industry_experience) {
		this.industry_experience = industry_experience;
	}

	public String getBrand_operation() {
		return brand_operation;
	}

	public void setBrand_operation(String brand_operation) {
		this.brand_operation = brand_operation;
	}

	public String getMain_site_address() {
		return main_site_address;
	}

	public void setMain_site_address(String main_site_address) {
		this.main_site_address = main_site_address;
	}

	public String getAlternate_site_address() {
		return alternate_site_address;
	}

	public void setAlternate_site_address(String alternate_site_address) {
		this.alternate_site_address = alternate_site_address;
	}

	public String getMain_site_source() {
		return main_site_source;
	}

	public void setMain_site_source(String main_site_source) {
		this.main_site_source = main_site_source;
	}

	public String getAlternate_site_source() {
		return alternate_site_source;
	}

	public void setAlternate_site_source(String alternate_site_source) {
		this.alternate_site_source = alternate_site_source;
	}

	public String getForm_status() {
		return form_status;
	}

	public void setForm_status(String form_status) {
		this.form_status = form_status;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public Date getSubmitted_date() {
		return submitted_date;
	}

	public void setSubmitted_date(Date submitted_date) {
		this.submitted_date = submitted_date;
	}

	public Date getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	public String getProvince_code() {
		return province_code;
	}

	public void setProvince_code(String province_code) {
		this.province_code = province_code;
	}

	public String getCity_code() {
		return city_code;
	}

	public void setCity_code(String city_code) {
		this.city_code = city_code;
	}

	public String getDistrict_code() {
		return district_code;
	}

	public void setDistrict_code(String district_code) {
		this.district_code = district_code;
	}

	public String getRecruitment_type() {
		return recruitment_type;
	}

	public void setRecruitment_type(String recruitment_type) {
		this.recruitment_type = recruitment_type;
	}

	public String getUnique_code() {
		return unique_code;
	}

	public void setUnique_code(String unique_code) {
		this.unique_code = unique_code;
	}

}