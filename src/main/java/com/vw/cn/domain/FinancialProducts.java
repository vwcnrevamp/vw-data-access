package com.vw.cn.domain;

import java.util.Date;
/**
 * @author karthikeyan_v
 */
public class FinancialProducts {

	private long id;

	private String code;

	private String financial_institution;

	private String name;

	private String description;

	private String lang;

	private boolean delete_flag;

	private Date last_updated_date;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFinancial_institution() {
		return financial_institution;
	}

	public void setFinancial_institution(String financial_institution) {
		this.financial_institution = financial_institution;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isDelete_flag() {
		return delete_flag;
	}

	public void setDelete_flag(boolean delete_flag) {
		this.delete_flag = delete_flag;
	}

	public Date getLast_updated_date() {
		return last_updated_date;
	}

	public void setLast_updated_date(Date last_updated_date) {
		this.last_updated_date = last_updated_date;
	}
}