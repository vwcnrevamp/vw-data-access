package com.vw.cn.domain;

public class GenericDetails {

	private int id;

	private String name;

	private String discriminator;

	private String description;

	private float value;

	private String extrafield;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDiscriminator() {
		return discriminator;
	}
	public void setDiscriminator(String discriminator) {
		this.discriminator = discriminator;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	public String getExtrafield() {
		return extrafield;
	}
	public void setExtrafield(String extrafield) {
		this.extrafield = extrafield;
	}


}
