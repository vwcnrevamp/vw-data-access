package com.vw.cn.domain;

import java.util.Date;
/**
 * @author karthikeyan_v
 */
public class Loan {

	private long id;

	private String car_series;

	private String car_model;

	private String financial_institutions;

	private String financial_product;	

	private float total_payment;

	private float down_payment;

	private float loan_tenure;

	private float interest;	

	private String dealer_province;

	private String dealer_city;

	private long dealer;

	private String name;

	private String title;

	private String phone;

	private String expected_purchase_time;

	private String loan_status;

	private boolean delete_flag;

	private Date last_updated_date;

	private Date application_date;
	
	private long created_by;

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCar_series() {
		return car_series;
	}

	public void setCar_series(String car_series) {
		this.car_series = car_series;
	}

	public String getCar_model() {
		return car_model;
	}

	public void setCar_model(String car_model) {
		this.car_model = car_model;
	}

	public String getFinancial_institutions() {
		return financial_institutions;
	}

	public void setFinancial_institutions(String financial_institutions) {
		this.financial_institutions = financial_institutions;
	}

	public String getFinancial_product() {
		return financial_product;
	}

	public void setFinancial_product(String financial_product) {
		this.financial_product = financial_product;
	}

	public float getTotal_payment() {
		return total_payment;
	}

	public void setTotal_payment(float total_payment) {
		this.total_payment = total_payment;
	}

	public float getDown_payment() {
		return down_payment;
	}

	public void setDown_payment(float down_payment) {
		this.down_payment = down_payment;
	}

	public float getLoan_tenure() {
		return loan_tenure;
	}

	public void setLoan_tenure(float loan_tenure) {
		this.loan_tenure = loan_tenure;
	}

	public float getInterest() {
		return interest;
	}

	public void setInterest(float interest) {
		this.interest = interest;
	}

	public String getDealer_province() {
		return dealer_province;
	}

	public void setDealer_province(String dealer_province) {
		this.dealer_province = dealer_province;
	}

	public String getDealer_city() {
		return dealer_city;
	}

	public void setDealer_city(String dealer_city) {
		this.dealer_city = dealer_city;
	}

	public long getDealer() {
		return dealer;
	}

	public void setDealer(long dealer) {
		this.dealer = dealer;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getExpected_purchase_time() {
		return expected_purchase_time;
	}

	public void setExpected_purchase_time(String expected_purchase_time) {
		this.expected_purchase_time = expected_purchase_time;
	}

	public String getLoan_status() {
		return loan_status;
	}

	public void setLoan_status(String loan_status) {
		this.loan_status = loan_status;
	}

	public boolean isDelete_flag() {
		return delete_flag;
	}

	public void setDelete_flag(boolean delete_flag) {
		this.delete_flag = delete_flag;
	}

	public Date getLast_updated_date() {
		return last_updated_date;
	}

	public void setLast_updated_date(Date last_updated_date) {
		this.last_updated_date = last_updated_date;
	}

	public Date getApplication_date() {
		return application_date;
	}

	public void setApplication_date(Date application_date) {
		this.application_date = application_date;
	}	
}