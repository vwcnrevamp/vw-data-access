package com.vw.cn.domain;

public class ModelFinancialInterest {

	private String model_code;

	private String financial_product_code;

	private float interest_rate;

	private float customer_interest_rate;

	public String getModel_code() {
		return model_code;
	}

	public void setModel_code(String model_code) {
		this.model_code = model_code;
	}

	public String getFinancial_product_code() {
		return financial_product_code;
	}

	public void setFinancial_product_code(String financial_product_code) {
		this.financial_product_code = financial_product_code;
	}

	public float getInterest_rate() {
		return interest_rate;
	}

	public void setInterest_rate(float interest_rate) {
		this.interest_rate = interest_rate;
	}

	public float getCustomer_interest_rate() {
		return customer_interest_rate;
	}

	public void setCustomer_interest_rate(float customer_interest_rate) {
		this.customer_interest_rate = customer_interest_rate;
	}	
}
