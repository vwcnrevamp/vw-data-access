package com.vw.cn.domain;

import java.util.Date;
/**
 * @author karthikeyan_v
 */
public class Promotion {

	private long id;

	private long dealer_id;

	private String promotional_banner_image_url;

	private String promotion_page_url;

	private Date created_date;

	private Date valid_from;

	private Date valid_till;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDealer_id() {
		return dealer_id;
	}

	public void setDealer_id(long dealer_id) {
		this.dealer_id = dealer_id;
	}

	public String getPromotional_banner_image_url() {
		return promotional_banner_image_url;
	}

	public void setPromotional_banner_image_url(String promotional_banner_image_url) {
		this.promotional_banner_image_url = promotional_banner_image_url;
	}

	public String getPromotion_page_url() {
		return promotion_page_url;
	}

	public void setPromotion_page_url(String promotion_page_url) {
		this.promotion_page_url = promotion_page_url;
	}	

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getValid_from() {
		return valid_from;
	}

	public void setValid_from(Date valid_from) {
		this.valid_from = valid_from;
	}

	public Date getValid_till() {
		return valid_till;
	}

	public void setValid_till(Date valid_till) {
		this.valid_till = valid_till;
	}	
}
