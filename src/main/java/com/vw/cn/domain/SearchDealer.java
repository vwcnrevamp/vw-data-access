package com.vw.cn.domain;
/**
 * @author karthikeyan_v
 */
public class SearchDealer {

	private long id;

	private String city_code;

	private String province_code;

	private float latitude;

	private float longitude;

	private String formatted_address;

	private String website_url;

	private String dealer_type;

	private String name;

	private String city_name;

	private String province_name;

	private long service_rating;

	private String promotional_banner_image_url;

	private String promotion_page_url;

	private String contact_sales_hotline;

	private String contact_customer_hotline;

	private String contact_after_sales;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCity_code() {
		return city_code;
	}
	public void setCity_code(String city_code) {
		this.city_code = city_code;
	}
	public String getProvince_code() {
		return province_code;
	}
	public void setProvince_code(String province_code) {
		this.province_code = province_code;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public String getFormatted_address() {
		return formatted_address;
	}
	public void setFormatted_address(String formatted_address) {
		this.formatted_address = formatted_address;
	}
	public String getWebsite_url() {
		return website_url;
	}
	public void setWebsite_url(String website_url) {
		this.website_url = website_url;
	}
	public String getDealer_type() {
		return dealer_type;
	}
	public void setDealer_type(String dealer_type) {
		this.dealer_type = dealer_type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getProvince_name() {
		return province_name;
	}
	public void setProvince_name(String province_name) {
		this.province_name = province_name;
	}
	public long getService_rating() {
		return service_rating;
	}
	public void setService_rating(long service_rating) {
		this.service_rating = service_rating;
	}
	public String getPromotional_banner_image_url() {
		return promotional_banner_image_url;
	}
	public void setPromotional_banner_image_url(String promotional_banner_image_url) {
		this.promotional_banner_image_url = promotional_banner_image_url;
	}
	public String getPromotion_page_url() {
		return promotion_page_url;
	}
	public void setPromotion_page_url(String promotion_page_url) {
		this.promotion_page_url = promotion_page_url;
	}
	public String getContact_sales_hotline() {
		return contact_sales_hotline;
	}
	public void setContact_sales_hotline(String contact_sales_hotline) {
		this.contact_sales_hotline = contact_sales_hotline;
	}
	public String getContact_customer_hotline() {
		return contact_customer_hotline;
	}
	public void setContact_customer_hotline(String contact_customer_hotline) {
		this.contact_customer_hotline = contact_customer_hotline;
	}
	public String getContact_after_sales() {
		return contact_after_sales;
	}
	public void setContact_after_sales(String contact_after_sales) {
		this.contact_after_sales = contact_after_sales;
	}	
}
