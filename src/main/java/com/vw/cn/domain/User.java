package com.vw.cn.domain;

import java.util.Date;

import com.vw.cn.domain.User;
/**
 * @author karthikeyan_v
 */
public class User{

	private long id;

	private String account;

	private String password;

	private String account_type;

	private Date created_date;

	private Date last_login_date;

	private long is_enabled;

	private Date last_modified_date;

	private boolean delete_flag;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAccount_type() {
		return account_type;
	}

	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getLast_login_date() {
		return last_login_date;
	}

	public void setLast_login_date(Date last_login_date) {
		this.last_login_date = last_login_date;
	}

	public long getIs_enabled() {
		return is_enabled;
	}

	public void setIs_enabled(long is_enabled) {
		this.is_enabled = is_enabled;
	}

	public Date getLast_modified_date() {
		return last_modified_date;
	}

	public void setLast_modified_date(Date last_modified_date) {
		this.last_modified_date = last_modified_date;
	}

	public boolean isDelete_flag() {
		return delete_flag;
	}

	public void setDelete_flag(boolean delete_flag) {
		this.delete_flag = delete_flag;
	}		
}